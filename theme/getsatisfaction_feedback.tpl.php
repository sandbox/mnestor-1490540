<script type="text/javascript" charset="utf-8">
var feedback_widget_options_<?php print $hash; ?> = {};
feedback_widget_options_<?php print $hash; ?>.display = "<?php print $getsatisfaction_display; ?>";
feedback_widget_options_<?php print $hash; ?>.company = "<?php print $getsatisfaction_company; ?>";
<?php if (isset($getsatisfaction_placement) && $getsatisfaction_placement != ''): ?>
feedback_widget_options_<?php print $hash; ?>.placement = "<?php print $getsatisfaction_placement; ?>";
<?php endif; ?>
feedback_widget_options_<?php print $hash; ?>.color = "<?php print $getsatisfaction_company; ?>";
feedback_widget_options_<?php print $hash; ?>.style = "<?php print $getsatisfaction_widget_type; ?>";
<?php if (isset($getsatisfaction_product) && $getsatisfaction_product != ''): ?>
feedback_widget_options_<?php print $hash; ?>.product = "<?php print $getsatisfaction_product; ?>";
<?php endif; ?>
feedback_widget_options_<?php print $hash; ?>.tag = "<?php print $getsatisfaction_widget_tag; ?>";
feedback_widget_options_<?php print $hash; ?>.custom_css = "<?php print $getsatisfaction_widget_css; ?>";
feedback_widget_options_<?php print $hash; ?>.custom_ie_css = "<?php print $getsatisfaction_widget_cssie; ?>";
feedback_widget_options_<?php print $hash; ?>.limit = "<?php print $getsatisfaction_widget_topic_count; ?>";

var feedback_widget = new GSFN.feedback_widget(feedback_widget_options_<?php print $hash; ?>);
</script>