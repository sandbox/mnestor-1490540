<div id='gsfn_list_widget'>
<a href="https://getsatisfaction.com/fstopgear" class="widget_title"></a>
<div id='gsfn_content'>Loading...</div>
<div class='powered_by'>
<a href="https://getsatisfaction.com/"><img alt="Burst16" src="https://getsatisfaction.com/images/burst16.png" style="vertical-align: middle;" /></a>
<a href="<?php print $getsatisfaction_url; ?>" target="_blank" class="widget_title">GetSatisfaction for f-stop</a>
</div>
</div>
<script src="https://getsatisfaction.com/fstopgear/widgets/javascripts/ee4d3077c1/widgets.js" type="text/javascript"></script>
<script src="https://getsatisfaction.com/fstopgear/topics.widget?callback=gsfnTopicsCallback
<?php if ($getsatisfaction_widget_summary_length != 0): ?>
&amp;length=<?php print $getsatisfaction_widget_summary_length; ?>
<?php endif; ?>
&amp;limit=<?php print $getsatisfaction_widget_topic_count; ?>
&amp;sort=<?php print $getsatisfaction_widget_order; ?>
<?php if ($getsatisfaction_widget_show != 'all'): ?>
&amp;style=<?php print $getsatisfaction_widget_show; ?>
<?php endif; ?>
<?php if ($getsatisfaction_widget_topics_about == 'product' && isset($getsatisfaction_product)): ?>
&amp;product=<?php print $getsatisfaction_product; ?>
<?php elseif ($getsatisfaction_widget_topics_about == 'tag' && isset($getsatisfaction_widget_tag)): ?>
&amp;tag=<?php print $getsatisfaction_widget_tag; ?>
<?php endif; ?>
" type="text/javascript"></script>