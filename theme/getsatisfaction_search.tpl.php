<div id='gsfn_search_widget'>
<div class='gsfn_content'>
<form accept-charset='utf-8' action='https://getsatisfaction.com/<?php print $getsatisfaction_company; ?>' id='gsfn_search_form' method='get' onsubmit='gsfn_search(this); return false;'>
<div>
<?php if (!empty($getsatisfaction_widget_tag)) :?>
<input name='tag' type='hidden' value='<?php print $getsatisfaction_widget_tag; ?>' />
<?php endif; ?>
<?php if (!empty($getsatisfaction_product)) :?>
<input name='product' type='hidden' value='<?php print $getsatisfaction_product; ?>' />
<?php endif; ?>
<input name='style' type='hidden' value='<?php print $getsatisfaction_widget_show; ?>' />
<input name='limit' type='hidden' value='<?php print $getsatisfaction_widget_topic_count; ?>' />
<input name='utm_medium' type='hidden' value='widget_search' />
<input name='utm_source' type='hidden' value='widget_<?php print $getsatisfaction_company; ?>' />
<input name='callback' type='hidden' value='gsfnResultsCallback' />
<input name='format' type='hidden' value='widget' />
<label class='gsfn_label' for='gsfn_search_query'>Ask a question, share an idea, or report a problem.</label>
<input id='gsfn_search_query' maxlength='120' name='query' type='text' value='' />
<input id='continue' type='submit' value='Continue' />
</div>
</form>
<div id='gsfn_search_results' style='height: auto;'></div>
</div>
<div class='powered_by'>
<a href="https://getsatisfaction.com/"><img alt="Burst16" src="https://getsatisfaction.com/images/burst16.png" style="vertical-align: middle;" /></a>
<a href="<?php print $getsatisfaction_url; ?>" target="_blank" class="widget_title">GetSatisfaction for f-stop</a>
</div>
</div>
<script src="https://getsatisfaction.com/fstopgear/widgets/javascripts/cec05b9974/widgets.js" type="text/javascript"></script>